#!/bin/bash
arch="$(uname -m)"
proot=$(wget -O - https://dl-cdn.alpinelinux.org/alpine/edge/testing/$arch/ | grep "proot-static" | sed "s/^.*=\"//g;s/\".*//g")
wget -c "https://dl-cdn.alpinelinux.org/alpine/edge/testing/$arch/$proot" -O proot-static.apk
tar -xvf proot-static.apk
install -D ./usr/bin/proot.static /app/bin/proot
